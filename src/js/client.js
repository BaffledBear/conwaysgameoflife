import React from 'react'
import ReactDOM from 'react-dom'

// Activate React Dev Tools for Chrome
if (typeof window !== 'undefined') {
    window.React = React;
}

const deadCell = {
    backgroundColor: '#555',
    height: '7px',
    width: '7px',
    border: '1px solid black'
}

const aliveCell = {
    backgroundColor: '#990',
    height: '7px',
    width: '7px',
    border: '1px solid black'
}

const rowStyle = {
    display: 'inline-block'
}

const mods = [
    [-1, -1],
    [-1, 0],
    [-1, 1],
    [0, -1],
    [0, 1],
    [1, -1],
    [1, 0],
    [1, 1]
];

// Main layout of the page
class GameOfLife extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            width: this.props.defaultWidth,
            height: this.props.defaultHeight,
            run: true,
            updateInterval: this.props.defaultUpdateInterval,
            generations: 0,
            clearBoard: ''
        };
    }

    handlePause() {
        this.setState({run: !this.state.run});
    }

    handleChangeSize(size) {
        this.setState(size);
    }

    incrementGenerations() {
        this.setState({generations: this.state.generations + 1});
    }

    clearBoard() {
        this.setState({generations: 0, run: false});
        this.state.clearBoard();
    }

    updateClearBoard(func) {
        this.setState({clearBoard: func});
    }

    render() {
        return (
                <div>
                    <p>Conway's Game Of Life</p>
                    <p>Generations: {this.state.generations}</p>
                    <Board
                    width={this.state.width}
                    height={this.state.height}
                    run={this.state.run}
                    defaultUpdateInterval={this.state.updateInterval}
                    incrementGenerations={this.incrementGenerations.bind(this)}
                    updateClearBoard={this.updateClearBoard.bind(this)} />
                    <input type="button" onClick={this.handlePause.bind(this)} value="Pause" />
                    <input type="button" onClick={this.clearBoard.bind(this)} value="Clear" />

                    <BoardSizeButtons
                    onConfigSubmit={this.handleChangeSize.bind(this)} />
                </div>
            );
    }
}

// Buttons for configuring the size of the board.
class BoardSizeButtons extends React.Component {

    handle50X50(e) {
        this.props.onConfigSubmit({width: 50, height: 50});
    }

    handle70X70(e) {
        this.props.onConfigSubmit({width: 70, height: 70});
    }

    handle30X30() {
        this.props.onConfigSubmit({width: 30, height: 30});
    }

    render() {
        return (
            <div>
                <div className="sizeButtons">
                    <input type="button" value="30 x 30" onClick={this.handle30X30.bind(this)} />
                    <input type="button" value="50 x 50" onClick={this.handle50X50.bind(this)} />
                    <input type="button" value="70 x 70" onClick={this.handle70X70.bind(this)} />
                </div>
            </div>
            );
    }
}

// The board and controller for the game.
class Board extends React.Component {
    constructor(props) {
        super(props);
        // Creates a basic board with a random layout.
        var board = Array.apply(
            null,
            {length: this.props.width}
            ).map(
            (cello, oIndex) => {
                var arr = Array.apply(
                    null,
                    {length: this.props.height}
                    ).map((celli, iIndex) => {
                        var bool = (Math.floor(Math.random() * 5) === 1);
                        return {
                            alive: bool,
                            neighbors: 0,
                            styles: bool ? aliveCell : deadCell
                        }; }
                    );
                return arr;
            });
        // Set the starting state of the board.
        this.state = {
            width: this.props.width,
            height: this.props.height,
            board: board,
            updateInterval: this.props.defaultUpdateInterval,
        };
    }

    render() {
        var board = this.state.board.map((arr, hIndex) => {
            var row = arr.map((cell, wIndex) => {
                    return <Cell
                    key={hIndex+""+wIndex}
                    json={this.state.board[wIndex][hIndex]}
                    updateBoard={this.updateCell.bind(this)}
                    getState={this.getState.bind(this)}
                    location={[wIndex, hIndex]} />;
            });
            return <div key={hIndex} className="row" style={rowStyle}>{row}</div>;
        });
        return (<div>{board}</div>);
    }

    // Clear the board and set the state of the board to all dead cells
    clearBoard() {
        var board = Array.apply(
            null,
            {length: this.state.width}
            ).map(
            (cello, oIndex) => {
                var arr = Array.apply(
                    null,
                    {length: this.state.height}
                    ).map((celli, iIndex) => {
                        return {
                            alive: false,
                            neighbors: 0,
                            styles: deadCell,
                            changed: true
                        }; }
                    );
                return arr;
            });
        this.setState({board: board});
    }

    // Returns the state of the requested cell.
    getState(location) {
        return this.state.board[location[0]][location[1]];
    }

    // Cycle through all cells and update neighboring cells counts
    // if the cell is alive. The call to update the state of the cells
    updateNeighbors() {
        // If this.props.run is false, return and don't run this.
        if (!this.props.run) {
            return;
        }

        // Create a new board
        var board = Array.apply(
            null,
            {length: this.props.width}
            ).map(
            (cello, oIndex) => {
                var arr = Array.apply(
                    null,
                    {length: this.props.height}
                    ).map((celli, iIndex) => {
                        return {
                            alive: false,
                            neighbors: 0,
                            styles: deadCell,
                            changed: false
                        }; }
                    );
                return arr;
            });
        // Update the neighboring cells and set the location to true
        for (var col = 0; col < this.state.board.length && col < this.props.width; col++) {
            for (var row = 0; row < this.state.board[0].length && row < this.props.height; row++) {
                if (this.state.board[col][row].alive) {
                    for(var m in mods) {
                        var rI = (+row) + (+mods[m][0]);
                        var cI = (+col) + (+mods[m][1]);
                        if (rI < 0) {
                            rI = board[0].length - 1;
                        } else if (rI >= board[0].length) {
                            rI = 0;
                        }
                        if (cI < 0) {
                            cI = board.length -1;
                        } else if (cI >= board.length) {
                            cI = 0;
                        }
                        board[cI][rI].neighbors++;
                        board[col][row].alive = true;
                        board[col][row].styles = aliveCell;
                    }
                }
            }
        }
        // Call the updateAlive funciton, using the temporary board to be updated
        board = this.updateAlive(board);
        this.setState({board: board, width: this.props.width, height: this.props.height});
        this.props.incrementGenerations();

    }

    // Cycle through the cells in the provided board and update their alive state
    // based on the determined criteria.
    updateAlive(board) {
        for (var col = 0; col < this.state.board.length && col < this.props.width; col++) {
            for (var row = 0; row < this.state.board[0].length && row < this.props.height; row++) {
                if (board[col][row].alive &&
                    board[col][row].neighbors < 2 ||
                    board[col][row].neighbors > 3) {
                    board[col][row].alive = false;
                    board[col][row].styles = deadCell;
                    board[col][row].changed = true;
                } else if ( !board[col][row].alive &&
                    board[col][row].neighbors === 3) {
                    board[col][row].alive = true;
                    board[col][row].styles = aliveCell;
                    board[col][row].changed = true;
                } else {
                    board[col][row].changed = false;
                }
            }
        }
        return board;
    }

    // Provide individual cells the option to update their state
    // remotely. This is used for handling clicking on a cell.
    updateCell(alive, location) {
        var board = this.state.board.slice();
        board[location[0]][location[1]].alive = alive;
        board[location[0]][location[1]].styles = aliveCell;
        board[location[0]][location[1]].changed = true;
        this.setState({board: board});
    }

    componentDidMount() {
        this.updateNeighbors();
        this.props.updateClearBoard(this.clearBoard.bind(this));
        setInterval(this.updateNeighbors.bind(this), this.props.defaultUpdateInterval);
    }

}

// Renders a single cell with the ability to react to a click.
class Cell extends React.Component {

    changeState() {
        var state = this.props.getState(this.props.location);
        if (state.alive) {
            this.props.updateBoard(false, this.props.location);
        } else {
            this.props.updateBoard(true, this.props.location);
        }
    }

    shouldComponentUpdate() {
        return this.props.getState(this.props.location).changed;
    }

    render() {
        return (
            <div
            style={this.props.json.styles}
            onClick={this.changeState.bind(this)}></div>
            );
    }
}

ReactDOM.render(
    <GameOfLife defaultHeight={50} defaultWidth={50} defaultUpdateInterval={100} />,
    document.getElementById("game")
);
